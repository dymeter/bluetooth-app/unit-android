import { Component, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BLE } from '@ionic-native/ble/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import EscPosEncoder from 'esc-pos-encoder-ionic';
import { TranslateService } from '@ngx-translate/core';
import { SupportService } from 'src/app/providers/services/support.service';

// Bluetooth UUIDs
const UUID_SERVICE = 'FFF0';
const WRITE_CHARACTERISTIC = 'FFF2';
const READ_CHARACTERISTIC = 'FFF1';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.page.html',
  styleUrls: ['./connect.page.scss'],
})
export class ConnectPage {

  peripheral: any = {};
  realtime: string = '0';
  pressure: number[] = [0, 0];
  private interval = null;
  message = '프린터 테스트입니다.';
  isConnected = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ble: BLE,
    private toastCtrl: ToastController,
    private translate: TranslateService,
    private support: SupportService,
    private ngZone: NgZone
  ) {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    //this.support.showAlert(id);
    this.ble.connect(id).subscribe(
      peripheral => {
        this.interval = setInterval(() => {      
          this.onConnected(peripheral)
        }, 1*1000);
      },
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  onConnected(peripheral) {
    console.log('Connected to ' + (peripheral.name || peripheral.id));
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });

    var inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    this.ble
        .writeWithoutResponse(
          this.peripheral.id, 
          UUID_SERVICE, 
          WRITE_CHARACTERISTIC, 
          inputdata.buffer
        )
        .then(
          data => {
            this.subscribe();
          },
          err => {
            console.error(err);
          }
    );
  }

  subscribe() {
    this.ble
        .startNotification(this.peripheral.id, UUID_SERVICE, READ_CHARACTERISTIC).subscribe(
      data => {
        this.onValueChange(data[0]);
      },
      e => console.error('noti error'+ e)
    );
  }

  onValueChange(buffer: ArrayBuffer) {
    this.ngZone.run(() => {
      try {
        let data = this.bytesToString(buffer);
        if(data.length == 22) {
          this.pressure[0] = data.substring(6, 11);
          this.pressure[1] = data.substring(16, 21);
        } else {
          this.realtime = data.replace(/[^0-9]/g,'');          
        }
      } catch (e) {
        console.error(e);
      }
    });
  }

  async onDeviceDisconnected(peripheral) {
    const toast = this.toastCtrl.create({
      message: '장치를 연결할수 없습니다.',
      duration: 3000,
      position: 'bottom'
    });
  }

  sendMessage(message) {
    const encoder = new EscPosEncoder();
    const result = encoder.initialize();
    result
    .codepage('cp949')
    .line(message)
    .newline()
    .newline()
    .newline()

    this.printData(result.encode())
    .then((printStatus) => {
      console.log(printStatus);
    });
  }

  printData(data: Uint8Array) {
    return this.ble.writeWithoutResponse(
      this.peripheral.id, 
      UUID_SERVICE, 
      WRITE_CHARACTERISTIC, 
      data.buffer
    );
  }

  ionViewWillLeave() {
    clearInterval(this.interval);
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}


