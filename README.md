

#### 아이콘 복사
```
npm install -g cordova-res

cordova-res ios --skip-config --copy
cordova-res android --skip-config --copy
```


#### AndroidManifest.xml
```
      <application
        android:networkSecurityConfig="@xml/network_security_config">
```

#### network_security_config.xml
```
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
<base-config cleartextTrafficPermitted="true">
    <trust-anchors>
        <certificates src="system" />
    </trust-anchors>
</base-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">localhost</domain>
        <domain includeSubdomains="true">app.dymeter.com</domain>
        <domain includeSubdomains="true">app.dymeter.com:4000/api</domain>
        <domain includeSubdomains="true">app.dymeter.com:4001/api</domain>
    </domain-config>
</network-security-config>
```

#### EscPosEncoder 바꾸기
```
start() {
    this._queue([
        0x1E, 0x53,
    ]);

    return this;
}

end() {
    this._queue([
        0x1E, 0x45,
    ]);

    return this;
}

korean() {
    this._queue([
        0x1E, 0x4C, 0x30,
    ]);

    return this;
}

english() {
    this._queue([
        0x1E, 0x4C, 0x31,
    ]);

    return this;
}
```

```
androidx.core.content.FileProvider

```