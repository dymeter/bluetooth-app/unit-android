import { BLE } from '@ionic-native/ble/ngx';
import { Component, NgZone, OnInit } from '@angular/core';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.scss'],
})
export class ScanComponent implements OnInit {
  resultId;
  bType;
  devices: any[] = [];
  statusMessage: string;

  constructor(
    public modalCtrl: ModalController,
    public router: Router, 
    private toastCtrl: ToastController,
    private ble: BLE,
    private ngZone: NgZone,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.scan();
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }


  scan() {
    this.setStatus('Scanning for Bluetooth LE Devices');
    this.devices = [];  // clear list

    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );

    setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    this.ngZone.run(() => {
      if(device.name == this.bType) {
        this.devices.push(device);
      }
    });
  }

  async scanError(error) {
    this.setStatus('Error ' + error);
    const toast = this.toastCtrl.create({
      message: 'Error scanning for Bluetooth low energy devices',
      position: 'middle',
      duration: 5000
    });
  }

  setStatus(message) {
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  deviceSelected(device) {
    this.dismiss();
    if(this.bType == 'YJ-300T') {
      this.router.navigate([`/app/details-print/${this.resultId}`, {device: device.id}]);
    }
  }
}
