import { fromEvent, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController, NavParams, AlertController, Platform } from '@ionic/angular';
import { Component, NgZone, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { GetApiService } from 'src/app/providers/services/get-api.service';
import { SupportService } from 'src/app/providers/services/support.service';
import { ResultComponent } from '../result/result.component';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.component.html',
  styleUrls: ['./measure.component.scss']
})
export class MeasureComponent implements OnInit, OnDestroy {

  @ViewChild('modelInput2', {static: false}) modelInput2;
  @ViewChild('modelInput3', {static: false}) modelInput3;

  UUID_SERVICE = environment.uuid_service;
  WRITE_CHARACTERISTIC = environment.write_characteristic;
  READ_CHARACTERISTIC = environment.read_characteristic;

  DEVICEID = environment.device_id;
  scaleUUID: string;
  airmeterUUID: string;

  form: FormGroup;
  mixs: any;
  isMix: boolean = false;
  realtime: string = '0';
  pressure: string[] = ['0', '0'];
  inputs: string[] = ['0', '0'];

  isConnected = false;
  private interval = null;
  
  peripheral: any = {};
  device = {};

  private backbuttonSubscription: Subscription;

  constructor(
    public modalCtrl: ModalController,
    public activateroute: ActivatedRoute,
    public navParams: NavParams,
    public getapi: GetApiService,
    private formBuilder: FormBuilder,
    public support: SupportService,
    private storage: Storage,
    private translate: TranslateService,
    public router: Router,
    public alertCtrl: AlertController,
    private ble: BLE,
    private ngZone: NgZone,
    private platform: Platform,
  ) {
    const id = this.navParams.get('id');
    this.getapi.mixDetails(id).subscribe((res: any) => {
      this.mixs = res;
      this.isMix = true;
    });
  }
  
  ionViewWillEnter(){
    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.bluetoothUUID(device).subscribe((res) => {
        this.scaleUUID = res[0].scale_uuid;
        this.airmeterUUID = res[0].airmeter_uuid;
        //this.checkConnection(this.scaleUUID);

      });
    });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      slump: [''],
      temp: [''],
      concrete: ['', [Validators.required]],
      water: ['', [Validators.required]],
      iPressure: ['', [Validators.required]],
      ePressure: ['', [Validators.required]],
    });

    /*
    const event = fromEvent(document, 'backbutton');
    this.backbuttonSubscription = event.subscribe(async () => {
        const modal = await this.modalCtrl.getTop();
        if (modal) {
          this.platform.backButton.observers.pop();
        }
    });
    */
    /*
    this.backbuttonSubscription = this.platform.backButton.subscribe(async ($event) => {
      this.presentDismiss();
    });
    */
  }

  ngOnDestroy() {
    this.backbuttonSubscription.unsubscribe();
  }
  
  ionViewWillLeave(){
    this.disconnected();
  }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }

  async checkConnection(seleccion) {
    await this.disconnected();
    this.ble.connect(seleccion).subscribe(
      peripheral => {
        this.ngZone.run(() => {
          this.interval = setInterval(() => {      
            this.onConnected(peripheral)
          }, 1*1000);
        });
        this.support.presentToast('장치와 연결되었습니다.');
        this.isConnected = true;
      },
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  onConnected(peripheral) {
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });
    
    var inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    
    this.ble
        .writeWithoutResponse(
          this.peripheral.id, 
          this.UUID_SERVICE, 
          this.WRITE_CHARACTERISTIC, 
          inputdata.buffer
        )
        .then(
          data => {
            this.subscribe();
          },
          err => {
            console.error(err);
          }
    );
  }

  subscribe() {
    this.ble
        .startNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC).subscribe(
      data => {
        this.onValueChange(data[0]);
      },
      e => console.error('noti error'+ e)
    );
  }

  onValueChange(buffer: ArrayBuffer) {
    try {
      let data = this.support.bytesToString(buffer);
      if(data.length == 14) {
        this.ngZone.run(() => {
          this.realtime = data.replace(/[^0-9]/g,'');          
        });
      } else if(data.length == 22) {
        this.ngZone.run(() => {
          this.pressure[0] = data.substring(6, 11);
          this.pressure[1] = data.substring(16, 21);
        });
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  async onDeviceDisconnected(peripheral) {
    this.support.presentToast('장치를 연결할수 없습니다.');
    let alert = this.alertCtrl.create({
      message: '장비와 연결을 실패했습니다. \n전원을 켜시고 재연결 버튼을 누르세요.',
      header: this.translate.instant("ALERT"),
      buttons: [
        {
          text: this.translate.instant("RECONNECT"),
          role: 'submit',
          handler: _ => {
            this.ble.stopNotification(peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC)
            .catch(err => console.error(err));
          }
        }
      ]
    });
    alert.then(alert => alert.present());
    this.isConnected = false;
  }

  async presentResult() {
    await this.dismiss();
    const modal = await this.modalCtrl.create({
      component: ResultComponent,
      cssClass: 'fullscreen',
      componentProps: { inputs: this.form.value, mixs: this.mixs }
    });
    await modal.present();
  }

  chooseAlert() {
    this.support.showAlert(this.translate.instant('SENTENCE_FIRST'));
  }

  apply() {
    this.inputs[0] = this.realtime;
    this.modelInput2.setFocus();
  }

  apply2() {
    this.inputs[1] = this.realtime;
    this.modelInput3.setFocus();
  }

  onEvent(ev) {
    let name = ev.target.name;
    switch(name) {
      case 'concrete':      
        this.checkConnection(this.scaleUUID);
        break;
      case 'water':
        break;
      case "iPressure":
        this.checkConnection(this.airmeterUUID);
        break;
      case "ePressure":
        break;
    }
  }

  dismiss() {
    this.disconnected();
    this.modalCtrl.dismiss();
  }

  disconnected() {
    clearInterval(this.interval);
    this.ble.stopNotification(this.peripheral.id, this.UUID_SERVICE, this.READ_CHARACTERISTIC)
    .catch(err => console.error(err));
    this.ble.disconnect(this.peripheral.id).then(
      () => {
        this.isConnected = false;
        console.log('Disconnected ' + JSON.stringify(this.peripheral, null, 2));
      },
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral, null, 2))
    )
  }

  presentDismiss() {
    let alert = this.alertCtrl.create({
      message: '이 페이지에서 나가시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
        },
        {
          text: '확인',
          handler: () => {
            this.dismiss();
          }
        }
      ]
    });
    alert.then(alert => alert.present());
  }
}
